from langchain_community.vectorstores import Chroma
from langchain_community.chat_models import ChatOllama
from langchain_community.embeddings import FastEmbedEmbeddings
from langchain.schema.output_parser import StrOutputParser
from langchain_community.document_loaders import PyPDFLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.schema.runnable import RunnablePassthrough
from langchain.prompts import PromptTemplate, prompt
from langchain_community.vectorstores.utils import filter_complex_metadata
from langchain.callbacks.manager import CallbackManager
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler
from langchain.memory import ConversationBufferMemory
from langchain.callbacks.tracers import ConsoleCallbackHandler
from langchain.chains import ConversationChain
from langchain.prompts.chat import (
    ChatPromptTemplate,
    HumanMessagePromptTemplate,
    SystemMessagePromptTemplate,
)

class MyChat:
    vector_store = None
    retriever = None
    chain = None

    def __init__(self):
        self.model = ChatOllama(
            model="orca-mini",
            verbose=True,
        )
        # self.text_splitter = RecursiveCharacterTextSplitter(chunk_size=1024, chunk_overlap=100)

        self.prompt = PromptTemplate.from_template(
            """
            <s> [INST] You are an assistant for question-answering tasks. Use the following pieces of retrieved context
            to answer the question. If you don't know the answer, just say that you don't know. Use three sentences
             maximum and keep the answer concise. [/INST] </s>
            [INST] Question: {question}
            Context: {context}
            Answer: [/INST]
            """
        )
        self.memory = ConversationBufferMemory()

        self.sys_prompt = SystemMessagePromptTemplate.from_template(
            """
            You are a french speaking IA. You are here to entertain the Human through the conversation. Here is the list of the exanges :

            {history}

            """
        )

        self.human_prompt = HumanMessagePromptTemplate.from_template("{input}")

        self.chat_prompt = ChatPromptTemplate.from_messages([self.sys_prompt, self.human_prompt])


    # def ingest(self, pdf_file_path: str):
    #     docs = PyPDFLoader(file_path=pdf_file_path).load()
    #     chunks = self.text_splitter.split_documents(docs)
    #     chunks = filter_complex_metadata(chunks)
    #
    #     vector_store = Chroma.from_documents(documents=chunks, embedding=FastEmbedEmbeddings())
    #     self.retriever = vector_store.as_retriever(
    #         search_type="similarity_score_threshold",
    #         search_kwargs={
    #             "k": 3,
    #             "score_threshold": 0.5,
    #         },
    #     )
    #
    #     self.chain = ({"context": self.retriever, "question": RunnablePassthrough()}
    #                   | self.prompt
    #                   | self.model
    #                   | StrOutputParser())

    def ask(self, query: str):
        self.conversation = ConversationChain(
            prompt=self.chat_prompt,
            llm=self.model,
            verbose=True,
            memory=self.memory
        )
        return self.conversation.predict(input=query)

    def clear(self):
        self.vector_store = None
        self.retriever = None
        self.chain = None
